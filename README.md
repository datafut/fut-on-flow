# Fut on flows


**A Encore librairy implementing control flow futures on top of dataflow explicit futures**
_version 1_


This repo is a compilation of various [Encore](https://github.com/parapluu/encore/) programs, which uses the [Data-flow explicit Futures version of Encore](https://gitlab.inria.fr/lhenrio/encorewithdatafuts/), including:
 - Different Control-Flow explicit futures librairy, in `/src/Futures`, the main one being in the file `/src/Futures/Futh`
 - Various examples and benchmarks of these different futures implementations, in in `examples`
 - Some benchmarking utils, in `src/Utils`

This has been a working repository (i.e. not clean), so it may be possible that the current version of the Futures librairy and the benchmarking tools are not compatible with every example in the librairy. 
