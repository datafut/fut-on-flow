WORKDIR="${FUTH_SOURCE}/examples/BigHash"
ENC="$HOME/workspace/cash/encore/release/encorec -I /home/hip/workspace/cash/fut-on-flow/src -F -latomic -O3"

echo "Compiling WithFut ..."
$ENC ${WORKDIR}/main.enc
echo "Compiling WithFlow ..."
$ENC $WORKDIR/WithFlow/main.enc
echo "Compiling WithFuth ..."
$ENC $WORKDIR/WithFuth/main.enc

chmod +x $WORKDIR/main
chmod +x $WORKDIR/WithFlow/main
chmod +x $WORKDIR/WithFuth/main

echo "Running WithFut ..."
unbuffer $WORKDIR/main
echo "Running WithFlow ..."
unbuffer $WORKDIR/WithFlow/main
echo "Running WithFuth ..."
unbuffer $WORKDIR/WithFuth/main

echo "Cleaning ..."
rm $WORKDIR/main $WORKDIR/WithFlow/main $WORKDIR/WithFuth/main -f
