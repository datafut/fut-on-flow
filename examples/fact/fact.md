# Fact example

On compare les performances de 2 implémentations de la factorielle avec 3 systèmes de futures.

## Sample outputs

```
The function fact_term_fut took 26 mms to run, and returned: 1409286144
The function fact_term_futh took 17 mms to run, and returned: 1409286144
The function fact_term_flow took 27 mms to run, and returned: 1409286144
The function fact_basic_futh took 590 mms to run, and returned: 1409286144
The function fact_basic_flow took 636 mms to run, and returned: 1409286144
The function fact_basic_fut took 622 mms to run, and returned: 1409286144
```

```
The function fact_term_fut took 18 mms to run, and returned: 1409286144
The function fact_term_futh took 16 mms to run, and returned: 1409286144
The function fact_term_flow took 24 mms to run, and returned: 1409286144
The function fact_basic_futh took 589 mms to run, and returned: 1409286144
The function fact_basic_flow took 615 mms to run, and returned: 1409286144
The function fact_basic_fut took 616 mms to run, and returned: 1409286144
```

Pour l'instant pas de résultats probants.
