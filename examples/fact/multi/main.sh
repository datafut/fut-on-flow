WORKDIR="${FUTH_SOURCE}/examples/fact/multi"
ENC="$HOME/workspace/cash/encore/release/encorec -I /home/hip/workspace/cash/fut-on-flow/src -F -latomic -O3"

echo "Compiling flow.enc ..."
$ENC $WORKDIR/flow.enc 2> /dev/null
echo "Compiling flowb.enc ..."
$ENC $WORKDIR/flowb.enc 2> /dev/null
echo "Compiling flowf.enc ..."
$ENC $WORKDIR/flowf.enc 2> /dev/null
echo "Compiling futb.enc ..."
$ENC $WORKDIR/futb.enc 2> /dev/null
echo "Compiling futf.enc ..."
$ENC $WORKDIR/futf.enc 2> /dev/null
echo "Compiling futhb.enc ..."
$ENC $WORKDIR/futhb.enc 2> /dev/null
echo "Compiling futhf.enc ..."
$ENC $WORKDIR/futhf.enc 2> /dev/null

echo "Running flow ..."
unbuffer $WORKDIR/flow
echo "Running flowb ..."
unbuffer $WORKDIR/flowb
echo "Running flowf ..."
unbuffer $WORKDIR/flowf
echo "Running futb ..."
unbuffer $WORKDIR/futb
echo "Running futf ..."
unbuffer $WORKDIR/futf
echo "Running futhb ..."
unbuffer $WORKDIR/futhb
echo "Running futhf ..."
unbuffer $WORKDIR/futhf

echo "Cleaning ..."

rm $WORKDIR/flow -f
rm $WORKDIR/flowb -f
rm $WORKDIR/flowf -f
rm $WORKDIR/futb -f
rm $WORKDIR/futf -f
rm $WORKDIR/futhb -f
rm $WORKDIR/futhf -f

echo "Done"
