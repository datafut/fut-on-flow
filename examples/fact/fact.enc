import String
import Task
import Futures.Futh
import Utils.Time

fun fact_term_fut(n: int, acc: int = 1): Fut[int]
  match n with
    case 0 => async(acc)
    case _ => fact_term_fut(n - 1, acc * n)
  end
end

fun fact_basic_fut(n: int): Fut[int]
  match n with
    case 0 => async(1)
    case _ => async(get(fact_basic_fut(n - 1)) * n)
  end
end

fun fact_term_futh(n: int, acc: int = 1): Futh[int]
  match n with
    case 0 => asynch(acc)
    case _ => fact_term_futh(n - 1, acc * n)
  end
end

fun fact_basic_futh(n: int): Futh[int]
  match n with
    case 0 => asynch(1)
    case _ => asynch(geth(fact_basic_futh(n - 1)) * n)
  end
end

fun fact_term_flow(n: int, acc: int = 1): Flow[int]
  match n with
    case 0 => async*(acc)
    case _ => fact_term_flow(n - 1, acc * n)
  end
end

fun fact_basic_flow(n: int): Flow[int]
  match n with
    case 0 => async*(1)
    case _ => async*(get*(fact_basic_flow(n - 1)) * n)
  end
end

active class Main
  def main() : unit
    val printer = string_from_int
    val n = 1000
    val n_term = n * 100

    timer(fun () => get(fact_term_fut(n_term)), "fact_term_fut")
    timer(fun () => geth(fact_term_futh(n_term)), "fact_term_futh")
    timer(fun () => get*(fact_term_flow(n_term)), "fact_term_flow")
    timer(fun () => geth(fact_basic_futh(n)), "fact_basic_futh")
    timer(fun () => get*(fact_basic_flow(n)), "fact_basic_flow")
    timer(fun () => get(fact_basic_fut(n)), "fact_basic_fut")
  end
end
