WORKDIR="${FUTH_SOURCE}/examples/LinkedList"
ENC="$HOME/workspace/cash/encore/release/encorec -I /home/hip/workspace/cash/fut-on-flow/src -F -latomic -O3"

echo "Compiling with_futh.enc ..."
$ENC $WORKDIR/with_futh.enc 2> /dev/null
echo "Compiling with_futh_with_forward.enc ..."
$ENC $WORKDIR/with_futh_with_forward.enc 2> /dev/null
echo "Compiling with_fut.enc ..."
$ENC $WORKDIR/with_fut.enc 2> /dev/null
echo "Compiling with_flow.enc ..."
$ENC $WORKDIR/with_flow.enc 2> /dev/null
echo "Compiling with_flow_with_forward.enc ..."
$ENC $WORKDIR/with_flow_with_forward.enc 2> /dev/null
echo "Compiling with_fut_without_forward.enc ..."
$ENC $WORKDIR/with_fut_without_forward.enc 2> /dev/null

chmod +x $WORKDIR/with_futh
chmod +x $WORKDIR/with_futh_with_forward
chmod +x $WORKDIR/with_fut
chmod +x $WORKDIR/with_flow
chmod +x $WORKDIR/with_flow_with_forward
chmod +x $WORKDIR/with_fut_without_forward


echo "Running with_futh_with_forward ..."
unbuffer $WORKDIR/with_futh_with_forward
echo "Running with_futh ..."
unbuffer $WORKDIR/with_futh
echo "Running with_fut ..."
unbuffer $WORKDIR/with_fut
echo "Running with_flow ..."
unbuffer $WORKDIR/with_flow
echo "Running with_flow_with_forward ..."
unbuffer $WORKDIR/with_flow_with_forward
echo "Running with_fut_without_forward ..."
unbuffer $WORKDIR/with_fut_without_forward

echo "Cleaning ..."

rm $WORKDIR/with_futh_with_forward -f
rm $WORKDIR/with_futh -f
rm $WORKDIR/with_fut -f
rm $WORKDIR/with_flow -f
rm $WORKDIR/with_flow_with_forward -f
rm $WORKDIR/with_fut_without_forward -f
