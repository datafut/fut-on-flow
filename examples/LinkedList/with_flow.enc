import Task
import String
import Utils.Time

active class Node
  val state: int
  val next: Maybe[Node]

  def init(s: int, n: Maybe[Node]): unit
    this.state = s
    this.next = n
  end

  def sum_basic(): Flow[int]
    let result = this.state in
      match this.next with
        case Just(next) => result + get*(next!!sum_basic())
        case _          => result
      end
    end
  end

  def sum_term(acc: int): Flow[int]
    let result = this.state + acc in
      match this.next with
        case Just(next) => next!!sum_term(result)
        case _          => result
      end
    end
  end
end

fun generate(n: int): Node
  if n == 0 then
    new Node(n, Nothing)
  else
    new Node(n, Just(generate(n - 1)))
  end
end

active class Main
  def main() : unit
    val n = 10000
    val nodes = generate(n)
    -- _timer[int](fun () => get*(nodes !! sum_basic()), "withFlow.sum_basic")
    timer[int](fun () => get*(nodes !! sum_term(0)), "withFlow.sum_term")
    println("Expected result: {}", n * (n + 1) / 2)
  end
end
