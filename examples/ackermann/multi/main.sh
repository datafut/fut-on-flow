WORKDIR="${FUTH_SOURCE}/examples/ackermann/multi"
ENC="$HOME/workspace/cash/encore/release/encorec -I /home/hip/workspace/cash/fut-on-flow/src -F -latomic -O3"

echo "Compiling flow.enc ..."
$ENC $WORKDIR/flow.enc 2> /dev/null
echo "Compiling flowf.enc ..."
$ENC $WORKDIR/flowf.enc 2> /dev/null
echo "Compiling fut.enc ..."
$ENC $WORKDIR/fut.enc 2> /dev/null
echo "Compiling futf.enc ..."
$ENC $WORKDIR/futf.enc 2> /dev/null
echo "Compiling futh.enc ..."
$ENC $WORKDIR/futh.enc 2> /dev/null
echo "Compiling futhf.enc ..."
$ENC $WORKDIR/futhf.enc 2> /dev/null
echo "Compiling fast.enc ..."
$ENC $WORKDIR/fast.enc 2> /dev/null

echo "Running flow ..."
unbuffer $WORKDIR/flow
echo "Running flowf ..."
unbuffer $WORKDIR/flowf
echo "Running fut ..."
unbuffer $WORKDIR/fut
echo "Running futf ..."
unbuffer $WORKDIR/futf
echo "Running futh ..."
unbuffer $WORKDIR/futh
echo "Running futhf ..."
unbuffer $WORKDIR/futhf
echo "Running fast ..."
unbuffer $WORKDIR/fast

echo "Cleaning ..."

rm $WORKDIR/flow -f
rm $WORKDIR/flowf -f
rm $WORKDIR/fut -f
rm $WORKDIR/futf -f
rm $WORKDIR/futh -f
rm $WORKDIR/futhf -f
rm $WORKDIR/fast -f

echo "Done"
