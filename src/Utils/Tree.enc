module Tree

import Random

active class Tree
  val id: int
  var state: int
  var left: Maybe[Tree]
  var right: Maybe[Tree]

  def init(x: int, left: Maybe[Tree], right: Maybe[Tree]): unit
    this.id = x
    this.left = left
    this.right = right
    this.state = random(1000)
  end
end

active class Generator
  var counter: int = 0

  -- util that build a node with left and right, and increment counter
  def private construct(left: Maybe[Tree], right: Maybe[Tree]): Tree
    val result = new Tree(this.counter, left, right)
    this.counter += 1
    result
  end

  -- construct a leaf
  def leaf(): Tree
    this.construct(Nothing, Nothing)
  end

  -- construct a binary tree of height n
  def binary_tree(n: int): Tree
    match n with
      case 0 => this.leaf()
      case _ => this.construct(Just(this.binary_tree(n - 1)), Just(this.binary_tree(n - 1)))
    end
  end

  -- construct a peigne with n nodes
  def peigne(n: int): Tree
    match n with
      case 0 => this.leaf()
      case _ => this.construct(Just(this.peigne(n - 1)), Nothing)
    end
  end

  def private create_random_tree(n: int): Tree
    val m = random(n - 1)
    this.construct(this.random_tree(m), this.random_tree(n - m - 1))
  end

  def private random_bi_tree(): Tree
    match random(2) with
      case 1 => this.construct(Nothing, Just(this.leaf()))
      case 0 => this.construct(Just(this.leaf()), Nothing)
    end
  end

  -- generate a random_looking tree
  def random_tree(n: int): Maybe[Tree]
    match n with
      case 0 => Nothing
      case 1 => Just(this.leaf())
      case 2 => Just(this.random_bi_tree())
      case _ => Just(this.create_random_tree(n))
    end
  end
end
